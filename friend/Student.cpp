#include"Student.h"
#include<iostream>
#include<string>

using namespace std;

Student::Student()
{
	imie = "Jas";
	nazwisko = "Fasola";
	oceny = new float[3];
	oceny[0] = 3.5;
	oceny[1] = 4.0;
	oceny[2] = 2.0;

	licznik++;                                   //czy sttyczne sie podawalo w konstruktorze?
}
Student::Student(string imie, string nazwisko, float* oceny)
{
	licznik++;
	this->imie = imie;
	this->nazwisko = nazwisko;
	this->oceny = new float[3];
	for (int i = 0; i < 3; i++)
	{
		this->oceny[i] = oceny[i];
	}
}
Student::Student(const Student & wzor)
{
	licznik++;
	this->imie = wzor.imie;
	this->nazwisko = wzor.nazwisko;
	this->oceny = new float[3];     // czy przez to jets potrzeny destruktor?
	for (int i = 0; i < 3; i++)
	{
		this->oceny[i] = wzor.oceny[i];
	}
}
int Student::getLicznik()
{
	return licznik;
}
void wypisz(Student student)
{
	cout << "imie i nazwisko: " << student.imie << ", " << student.nazwisko  << "  OCENY: ";   // ale w innej klasie a nie w funkci globalnej
	for (int i = 0; i < 3; i++)
	{
		cout << student.oceny[i];
		cout << ", ";
	}
	cout << endl;
}
int Student::licznik = 0;