#include<iostream>
#include"Student.h"
#include"Nauczyciel.h"
#include<string>
#include<cstdlib>
using namespace std;

Nauczyciel::Nauczyciel(): maks_liczba_studentow(15)
{
	liczba_studentow=0;
	this->tab_studentow = new Student[maks_liczba_studentow];
}
Nauczyciel::Nauczyciel(const int maks_liczba_studentow, Student* tab_studentow) : maks_liczba_studentow(15)
{
	liczba_studentow=0;
	this->tab_studentow = new Student[maks_liczba_studentow];
	for (int i = 0; i < maks_liczba_studentow; i++)
	{
		this->tab_studentow[i] = tab_studentow[i];
	}
}
Nauczyciel::Nauczyciel(const Nauczyciel & wzor): maks_liczba_studentow(wzor.maks_liczba_studentow)
{
	liczba_studentow=0;
	this->tab_studentow = new Student[maks_liczba_studentow];
	for (int i = 0; i < maks_liczba_studentow; i++)
	{
		this->tab_studentow[i] = wzor.tab_studentow[i];
	}
}
Nauczyciel::~Nauczyciel()
{
	liczba_studentow--;
	delete [] tab_studentow;
}
void Nauczyciel::ocenStudenta(Student &student1)
{
	for (int i = 0; i < 3; i++)
	{
		student1.oceny[i] = rand() % (4 + 7)/2.f;          // do poprawki
	}
}
float Nauczyciel::dajSrednia(Student &student1)
{
	float srednia;
	srednia = ((student1.oceny[0] + student1.oceny[1] + student1.oceny[2]) / 3);
	return srednia;
}
void Nauczyciel::wypisz_wszystkich()
{
	cout << "UCZNIOWE NAUCZYCIELA: " << endl;
	for (int i = 0; i< ; i++)
	{
		cout << i << ". "; 
		wypisz(tab_studentow[i]);
		cout << endl;
	}
}