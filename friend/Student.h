#pragma once
#include<iostream>
#include<string>

using namespace std;

class Nauczyciel;
class Student
{
	string imie;
	string nazwisko;
	static int licznik;
	float* oceny;
public:
	Student(); //
	Student(string, string, float*);
	Student(const Student & wzor);
	friend class Nauczyciel;
	friend void wypisz(Student student);
	friend void wypiszwszystkich();
	static int getLicznik();
};