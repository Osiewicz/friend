#pragma once
#include<iostream>
#include"Student.h"


class Nauczyciel
{
	Student* tab_studentow;
	const int maks_liczba_studentow;
	int liczba_studentow;
public:
	Nauczyciel();
	Nauczyciel(int maks_liczba_studentow, Student* tab_studentow);           //bona si ezpaytac
	Nauczyciel(const Nauczyciel &wzor);
	~Nauczyciel();

	void ocenStudenta(Student &student1);
	float dajSrednia(Student &student1);
	void wypisz_wszystkich();
};